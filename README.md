**M3**- Programació                                         

Eric Santos Cortada                                                                                             

Professor/a: Toni Pifarré Mata

**Exercici 12:** 

**Donat un text calcular la seva longitud.** 

![](Exercicis/ex12_UF2.png)

___

**Exercici 13:** 

**Donat un text comptar el nombre de vocals.** 

![](Exercicis/ex13_UF2.png)

___

**Exercici 14:**

**Donat un text comptar el nombre de consonants.** 

![](Exercicis/ex14_UF2.png)

___

**Exercici 15:** 

**Donat un text capgirar-lo.** 

![](Exercicis/ex15_UF2.png)

___

**Exercici 16:** 

**Donat un text comptar el nombre de paraules que acaben en “ts”.** 

![](Exercicis/ex16.1_UF2.png)&emsp;![](Exercicis/ex16.2_UF2.png)&emsp;![](Exercicis/ex16.3_UF2.png)

___

**Exercici 17:** 

**Donat un text comptar el nombre de paraules.** 

![](Exercicis/ex17.1_UF2.png)&emsp;![](Exercicis/ex17.2_UF2.png)&emsp;![](Exercicis/ex17.3_UF2.png)

___

**Exercici 18:** 

**Donat un text dissenyeu un algorisme que compti els cops que apareixen conjuntament la parella de caràcters “as” dins del text.** 
                                                                    
![](Exercicis/ex18_UF2.png)

___

**Exercici 19:** 

**Donat un text (text) i una paraula (parbus). Dissenyeu un algorisme que comprovi si la paraula és troba dins del text.** 

![](Exercicis/ex19.1_UF2.png)&emsp;![](Exercicis/ex19.2_UF2.png)

**Nota: parbus i text són el nom de les variables que contenen el text i la paraula
a cercar.** 

___

**Exercici 20:** 

**Donat un text i una paraula (parbus). Dissenyeu un algorisme que digui quants cops apareix la paraula (parbus) dins del text.**

![](Exercicis/ex20.1_UF2.png)&emsp;![](Exercicis/ex20.2_UF2.png)&emsp;![](Exercicis/ex20.3_UF2.png)

___

**Exercici 21:** 

**Donat un text, una paraula (parbus), i una paraula (parsub), dissenyeu un algorisme que substitueixi, en el text, totes les vegades que apareix la paraula (parbus) per la paraula (parsub).** 

![](Exercicis/ex21.1_UF2.png)&emsp;![](Exercicis/ex21.2_UF2.png)&emsp;![](Exercicis/ex21.3_UF2.png)

**Nota: parbus és el nom de la variable que conté la paraula a buscar i parsub és la paraula que volem substituir.** 
